<?php
namespace App\Helper;

class ProductsFilter
{
    public static function filterProducts($request, $products)
    {
        if (array_key_exists('brand', $request)) $products->where('brand', $request['brand']);
        if (array_key_exists('rating_min', $request)) $products->where('rating', '>=', $request['rating_min']);
        if (array_key_exists('rating_max', $request)) $products->where('rating', '<', $request['rating_max']);
        if (array_key_exists('price_min', $request)) $products->whereRaw("CAST(prices->>'$.actual' AS DECIMAL(10,6)) >= ".$request['price_min']);
        if (array_key_exists('price_max', $request)) $products->whereRaw("CAST(prices->>'$.actual' AS DECIMAL(10,6)) < ".$request['price_max']);

        return $products;
    }

    public static function orderByProducts($request)
    {
        $order_by = array_key_exists("order_by", $request) ? $request["order_by"] : "rating";
        $order_by_direction = array_key_exists("order_by_drc", $request) ? $request["order_by_drc"] : "asc";
        $order_by_statement = $order_by == "prices" ? "CAST(prices->>'$.sale' AS DECIMAL(10,6))" : $order_by;

        return "{$order_by_statement} {$order_by_direction}";
    }
}
