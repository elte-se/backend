<?php
namespace App\Helper;

class ReviewsFilter
{
    public static function orderByReviews($request)
    {
        $order_by = "likes";
        $order_by_direction = "desc";
        if(array_key_exists('order_by', $request))
            if ($request['order_by'] == 'recent') {
                $order_by = "created_at"; $order_by_direction = "desc";
            } elseif ($request['order_by'] == 'oldest') {
                $order_by = "created_at"; $order_by_direction = "asc";
            }
        return "{$order_by} {$order_by_direction}";
    }
}
