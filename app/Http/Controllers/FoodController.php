<?php

namespace App\Http\Controllers;

use App\Models\Food;
use Illuminate\Http\Request;

class FoodController extends Controller
{
    public function getFoods(Request $request)
    {
        try {
            $foods = Food::where($request->input("id"))->get();
            return response()->json($foods, 200);
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
