<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Response;
use App\Models\User;
use App\Models\Question;
use App\Models\Answer;
use Spatie\Searchable\Search;

class UserController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    function __construct() {
        //
    }


    public function register(Request $request)
    {
        try
        {
            $rules = [
                'name' => 'required|max:255',
                'surname' => 'required|max:255',
                'username' => 'required|unique:users',
                'password' => 'required|min:8',
            ];
            $this->validate($request, $rules);

            $fields = $request->all();
            $fields['password'] = \Hash::make($request->input('password'));

            $user = User::create($fields);
            return response()->json($user, 201);
        }
        catch (\Exception $e) {
            throw $e;
        }
    }

    public function editProfile(Request $request)
    {
        try {
            $fields = $request->only('name', 'surname', 'description','username');
            if(empty($fields)) $this->errorResponse('Bad Request', RESPONSE::HTTP_BAD_REQUEST);

            $user = User::where('id', Auth::user()->id)->update($fields);
            if(!$user) $this->errorResponse("Couldn't Updated", Response::HTTP_BAD_REQUEST);
            return response()->json($user, 200);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function myDetails()
    {
        try {
            $user = User::where('id', Auth::user()->id)->get();
            return response()->json($user, 200);
        } catch (\Exception $e) {
            throw $e;
        }
    }


}
