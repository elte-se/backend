<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

// Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

Route::group(
    [
        'namespace' => "App\Http\Controllers"
    ], function ($router) {

    Route::post('register', 'UserController@register');
    Route::get('recipes', 'RecipeController@getRecipes');
    Route::get('foods', 'FoodController@getFoods');
    Route::middleware('auth:api')->get('me', 'UserController@myDetails');
});
